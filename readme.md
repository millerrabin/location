# Location-Browser
The set of functions for working with browser location
 
## How to install
```bash
    npm install location-browser
```
    
###Usage
```javascript
    import location from '/node_modules/location-browser/main.js'    
```

### getSearch()
Return search part of location as object

```javascript
    const search = location.getSearch();
    console.log(search);
```

